
$(document).ready(function(){
        $("#registerUser").click(function(){
                var postdata = {};
                postdata["username"] = $("#username").val();
                postdata["firstname"] = $("#first_name").val();
                postdata["lastname"] = $("#last_name").val();
                postdata["password"] = $("#password").val();
                postdata["region"] = $("#region").val();
                postdata["email"] = $("#email").val();
                $.post("http://54.201.119.100:8080/registerUser", postdata, function(data){
                        console.log(data);
                }, "json");

        });

        $("#deleteUsers").click(function(){

                /*$.post("http://54.201.119.100:8080/deleteUsers", function(data){
                        console.log(data);
                }, "json");*/

                $.ajax({
                url: 'http://54.201.119.100:8080/deleteUsers',
                type: 'DELETE',
                success: function(result) {
                        console.log(result);
                        alert('The users have been deleted');
                },

                error: function(result) {
                        console.log(result);
                        alert('Something went wrong');
                }
                });



        });

        $("#logInB").click(function(){


                var postdata = {};
                postdata["username"] = $("#userNamelogIn").val();
                postdata["password"] = $("#passwordlogIn").val();

                $.post("http://54.201.119.100:8080/logIn", postdata, function(data){
                        console.log(data);
                }, "json");

                
        });

        $("#logOutB").click(function(){


                $.post("http://54.201.119.100:8080/logOut", function(data){
                        console.log(data);
                }, "json");

                
        });

        $("#deleteRowUsers").click(function(){

                var postdata = {};
                postdata["username"] = $("#delUsername").val();
                /*
                $.post("http://54.201.119.100:8080/deleteRowUsers", postdata, function(data){
                        console.log(data);
                }, "json");*/

                $.ajax({
                url: 'http://54.201.119.100:8080/deleteRowUsers',
                type: 'DELETE',
                data: postdata,
                success: function(result) {
                        console.log(result);
                        alert('One user has been deleted');
                },

                error: function(result) {
                        console.log(result);
                        alert('Something went wrong');
                }
                });


        });

        $("#send_message").click(function(){
                var postdata = {};
                postdata["user_to"] = $("#user_to").val();
                postdata["user_from"] = $("#user_from").val();
                postdata["message"] = $("#message_chat").val();

                
                $.post("http://54.201.119.100:8080/sendMessage", postdata, function(data){
                        console.log(data);
                }, "json");
        });

        $("#retrieve_message").click(function(){

        var user_to = $("#user_to").val();


        $.get("http://54.201.119.100:8080/getMessages?user_to=" +user_to, function(data){
                        console.log(data);
                }, "json");
        });


});

